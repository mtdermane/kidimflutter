import 'package:flutter/material.dart';
import '../models/type.dart';
import '../widgets/defaultLayout.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../pages/selectMealPage.dart';
import 'package:shimmer/shimmer.dart';

/// Class containing the item type template
class TypeItem extends StatelessWidget {
  final Type _typeName;
  final double _typeFontSize = 20.0;
  final double _leadingWidgetWidth = 50.0;
  final double _dividerMargin = 20.0;
  TypeItem(this._typeName);

  @override
  Widget build(BuildContext context) {
    final String _assetName = 'images/icon_type.svg';
    final Widget _svgType = new SvgPicture.asset(_assetName, semanticsLabel: 'icon type');
    return Column(
      children: <Widget>[
        ListTile(
          leading: Container(
            width: _leadingWidgetWidth,
            child: _svgType,
          ),
          title: Text(
            _typeName.typeName,
            style: TextStyle(
              fontFamily: 'Baskerville',
              fontSize: _typeFontSize,
            ),
          ),
          onTap: (){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => MenuPage(_typeName.typeName)),
            );
          },
        ),
        Container(
          margin: EdgeInsets.symmetric(vertical: 0.0, horizontal: _dividerMargin),
          child: Divider(
            color: Colors.grey[400],
          ),
        )
      ],
    );
  }
}

///Content widget

class TypeListWidget extends StatefulWidget {
  @override
  _TypeListWidgetState createState() => _TypeListWidgetState();
}

class _TypeListWidgetState extends State<TypeListWidget> {
  List<Type> _typeList = <Type>[
    new Type("Entree"),
    new Type("Plat de resistance"),
    new Type("Dessert"),
  ];
  double _cellHeight = 75.0;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: ListView.builder(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
        itemCount: _typeList.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            height: _cellHeight,
            child: TypeItem(
              _typeList[index],
            ),
          );
        },
      ),
    );
  }
}

///toolbar widget

class TypeListPage extends StatefulWidget {
  final String _placeId;

  TypeListPage(this._placeId);

  @override
  _TypeListPageState createState() => _TypeListPageState();
}

class _TypeListPageState extends State<TypeListPage> {
  @override
  Widget build(BuildContext context) {
    return DefaultLayout('Menu Type', TypeListWidget());
  }
}
