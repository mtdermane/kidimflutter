import 'package:flutter/material.dart';
import '../models/place.dart';
import '../widgets/defaultLayout.dart';

/// Class containing the item type template
///
class TypeItem extends StatelessWidget {
  final Place _placeName;
  final double _typeFontSize = 20.0;
  final double _dividerMargin = 20.0;
  TypeItem(this._placeName);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text(
            _placeName.name,
            style: TextStyle(
              fontFamily: 'Baskerville',
              fontSize: _typeFontSize,
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(vertical: 0.0, horizontal: _dividerMargin),
          child: Divider(
            color: Colors.grey[400],
          ),
        )
      ],
    );
  }
}

///place widget

class PlaceListWidget extends StatefulWidget {

  final List<Place> _placeList;

  PlaceListWidget(this._placeList);

  @override
  _PlaceListWidgetState createState() => _PlaceListWidgetState();
}

class _PlaceListWidgetState extends State<PlaceListWidget> {
  double _cellHeight = 75.0;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: ListView.builder(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
        itemCount: widget._placeList.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            height: _cellHeight,
            child: TypeItem(
              widget._placeList[index],
            ),
          );
        },
      ),
    );
  }
}

///Toolbar widget

class PlaceListPage extends StatefulWidget {
  final List<Place> _placeList;

  PlaceListPage(this._placeList);

  @override
  _PlaceListPageState createState() => _PlaceListPageState();
}

class _PlaceListPageState extends State<PlaceListPage> {
  @override
  Widget build(BuildContext context) {
    return DefaultLayout('Places list', PlaceListWidget(widget._placeList));
  }
}
