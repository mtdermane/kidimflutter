import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import "package:google_maps_webservice/places.dart";
import 'package:location/location.dart' as userLocation;
import '../models/place.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import '../pages/noPlacePage.dart';
import '../pages/yesPlacePage.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  String _googleApiKey = "AIzaSyDzTh679MhvFR3QXD4CamO71i1-dye0UeQ";

  double _buttonFontSize = 25.0;
  double _containerSpace = 20.0;
  double _buttonWidth = 200.0;
  double _mainFontSize = 100.0;
  double _secondaryFontSize = 40.0;
  double _tertiaryFontFontSize = 25.0;
  double _mainContainerMargin = 20.0;
  String _currentPLaceName = 'Mysterious place';
  bool _hasFoundPlace = false;
  List<Place> _placeList = <Place>[];

  static final _mainBackgroundAssetName = 'images/main_background.svg';
  final Widget _svgBackground = new SvgPicture.asset(
    _mainBackgroundAssetName,
    color: Colors.brown[200],
    semanticsLabel: 'Main background',
  );

  static final String _decorativeAssetName = 'images/decorative_line.svg';
  final Widget _svgDecorative = new SvgPicture.asset(
    _decorativeAssetName,
    color: Colors.brown[200],
    semanticsLabel: 'icon type',
  );

  ///get surroundings calls google places and request for the places around 500 meters
  Future getSurroundingPlaces() async {
    //getting the google api key
    final places = new GoogleMapsPlaces(apiKey: _googleApiKey);
    //request for the location
    var location = new userLocation.Location();
    userLocation.LocationData currentLocation;
    currentLocation = await location.getLocation();
    //getting the google place results
    PlacesSearchResponse response = await places.searchNearbyWithRadius(
        new Location(currentLocation.latitude, currentLocation.longitude), 500);
    if (_placeList.length == 0) {
      List<Place> listPlace = <Place>[];
      for (var i = 0;
          i < response.results.asMap().values.toList().length;
          i++) {
        var currentValue = response.results.asMap().values.toList()[i];
        listPlace.add(new Place(currentValue.id, currentValue.name));
      }

      setState(() {
        _hasFoundPlace = true;
        _currentPLaceName = listPlace[0].name;
        _placeList = listPlace;
      });
    }
  }

  Widget dynamicPlaceName(bool showDots) {
    if (showDots) {
      return Container(
        margin: EdgeInsets.symmetric(
            vertical: 0.0, horizontal: _mainContainerMargin),
        child: Text(
          _currentPLaceName,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'Baskerville',
            color: Colors.brown,
            fontSize: _tertiaryFontFontSize,
          ),
        ),
      );
    }

    return SpinKitThreeBounce(
      color: Colors.brown,
      size: 50.0,
    );
  }

  @override
  Widget build(BuildContext context) {
    getSurroundingPlaces();

    return Scaffold(
      backgroundColor: Colors.orange[50],
      body: SafeArea(
          child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Expanded(
                child: _svgBackground,
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.symmetric(
                    vertical: 0.0, horizontal: _mainContainerMargin),
                child: Text(
                  'Kidim',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'Aclonica',
                    color: Colors.red[600],
                    fontSize: _mainFontSize,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(
                    vertical: 0.0, horizontal: _mainContainerMargin),
                child: Text(
                  'Choose, Order, Enjoy',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'Aclonica',
                    color: Colors.red[600],
                    fontSize: _secondaryFontSize,
                  ),
                ),
              ),
              _svgDecorative,
              Container(
                margin: EdgeInsets.symmetric(vertical: 20.0, horizontal: 0.0),
              ),
              Container(
                margin: EdgeInsets.symmetric(
                    vertical: 0.0, horizontal: _mainContainerMargin),
                child: Text(
                  'Are you at the',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'Baskerville',
                    color: Colors.brown,
                    fontSize: _tertiaryFontFontSize,
                  ),
                ),
              ),
              dynamicPlaceName(_hasFoundPlace),
              Container(
                margin: EdgeInsets.symmetric(
                    horizontal: _mainContainerMargin,
                    vertical: _containerSpace),
                width: _buttonWidth,
                child: RaisedButton(
                  color: Colors.red[600],
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => TypeListPage(_placeList[0].id)),
                    );
                  },
                  child: Text(
                    'Yes',
                    style: TextStyle(
                        fontFamily: 'Baskerville',
                        fontSize: _buttonFontSize,
                        color: Colors.white),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(
                    vertical: 0.0, horizontal: _mainContainerMargin),
                width: _buttonWidth,
                child: RaisedButton(
                  color: Colors.red[600],
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => PlaceListPage(_placeList)),
                    );
                  },
                  child: Text(
                    'No',
                    style: TextStyle(
                      fontFamily: 'Baskerville',
                      color: Colors.white,
                      fontSize: _buttonFontSize,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      )),
    );
  }
}
