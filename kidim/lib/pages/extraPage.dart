import 'package:flutter/material.dart';
import '../models/extras.dart';
import '../widgets/defaultLayout.dart';

///Content widget

class ExtraListWidget extends StatefulWidget {
  @override
  _ExtraListWidgetState createState() => _ExtraListWidgetState();
}

class _ExtraListWidgetState extends State<ExtraListWidget> {
  List<Extras> _extraList = <Extras>[
    new Extras("Extra1", "10", false),
    new Extras("Extra2", "20", false),
    new Extras("Extra3", "30", false),
  ];
  double _extraSize = 15.0;
  final double _dividerMargin = 20.0;
  double _buttonFontSize = 25.0;
  double _mainContainerMargin = 20.0;
  double _buttonWidth = 200.0;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
                padding: const EdgeInsets.all(8.0),
                itemCount: _extraList.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    height: 75,
                    child: Column(
                      children: <Widget>[
                        CheckboxListTile(
                          value: _extraList[index].isSelected,
                          onChanged: (bool value) {
                            setState(() { _extraList[index].isSelected = value; });
                          },
                          activeColor: Colors.brown,
                          title: Text(
                            _extraList[index].price,
                            style: TextStyle(
                              fontFamily: 'Baskerville',
                              fontSize: _extraSize,
                            ),
                          ),
                          secondary: Text(
                            _extraList[index].name,
                            style: TextStyle(
                              fontFamily: 'Baskerville',
                              fontSize: _extraSize,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 0.0, horizontal: _dividerMargin),
                          child: Divider(
                            color: Colors.grey[400],
                          ),
                        )
                      ],
                    ),
                  );
                }),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: _mainContainerMargin),
            width: _buttonWidth,
            child: RaisedButton(
              color: Colors.red[600],
              onPressed: () {},
              child: Text(
                'Add',
                style: TextStyle(
                  fontFamily: 'Baskerville',
                  color: Colors.white,
                  fontSize: _buttonFontSize,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

///Toolbar widget

class ExtraPage extends StatefulWidget {
  
  @override
  _ExtraPageState createState() => _ExtraPageState();
}

class _ExtraPageState extends State<ExtraPage> {

  @override
  Widget build(BuildContext context) {
    return DefaultLayout("Extras", ExtraListWidget());
  }
}
