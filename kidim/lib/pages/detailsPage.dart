import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../models/option.dart';
import 'extraPage.dart';

///option list view details

class OptionView extends StatefulWidget {
  final List<Option> _optionList;

  OptionView(this._optionList);

  @override
  _OptionViewState createState() => _OptionViewState(_optionList);
}

class _OptionViewState extends State<OptionView> {
  List<Option> _optionList;

  _OptionViewState(this._optionList);
  final double _dividerMargin = 20.0;

  double _optionSize = 15.0;

  String _value;
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
          padding: const EdgeInsets.all(8.0),
          itemCount: _optionList.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              height: 75,
              child: Column(
                children: <Widget>[
                  RadioListTile(
                    value: _optionList[index].price,
                    groupValue: _value,
                    onChanged: (ind) => setState(() => _value = ind),
                    activeColor: Colors.brown,
                    title: Text(
                      _optionList[index].name,
                      style: TextStyle(
                        fontFamily: 'Baskerville',
                        fontSize: _optionSize,
                      ),
                    ),
                    secondary: Text(
                      _optionList[index].price,
                      style: TextStyle(
                        fontFamily: 'Baskerville',
                        fontSize: _optionSize,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 0.0, horizontal: _dividerMargin),
                    child: Divider(
                      color: Colors.grey[400],
                    ),
                  )
                ],
              ),
            );
          }),
    );
  }
}

///content widget

class DetailWidget extends StatefulWidget {
  @override
  _DetailWidgetState createState() => _DetailWidgetState();
}

class _DetailWidgetState extends State<DetailWidget> {
  double _buttonWidth = 200.0;
  double _buttonFontSize = 25.0;
  double _mainContainerMargin = 16;
  double _descriptionFontSize = 20.0;
  String _total = '0';

  List<Option> _optionList = <Option>[
    new Option("Option 1", "10"),
    new Option("Option 2", "20"),
    new Option("Option 3", "30"),
  ];

  static final String _assetName = 'images/other_decorative_line.svg';
  final Widget _svgDecorative =
      new SvgPicture.asset(_assetName, semanticsLabel: 'icon type');

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(
            vertical: _mainContainerMargin,
            horizontal: _mainContainerMargin,
          ),
          width: _buttonWidth,
          child: RaisedButton(
            color: Colors.red[600],
            onPressed: () {},
            child: Text(
              'Add',
              style: TextStyle(
                fontFamily: 'Baskerville',
                color: Colors.white,
                fontSize: _buttonFontSize,
              ),
            ),
          ),
        ),
        Text(
          'Total : ' + '\$' + _total,
          style: TextStyle(
            fontFamily: 'Baskerville',
            fontSize: _buttonFontSize,
          ),
        ),
        _svgDecorative,
        Container(
          margin: EdgeInsets.symmetric(
            vertical: _mainContainerMargin,
            horizontal: _mainContainerMargin,
          ),
          child: Text(
            'No description available',
            maxLines: 4,
            style: TextStyle(
              fontFamily: 'Baskerville',
              fontSize: _descriptionFontSize,
            ),
          ),
        ),
        _svgDecorative,
        OptionView(_optionList)
      ],
    );
  }
}

///Toolbar layout

class DetailPage extends StatefulWidget {
  final String _title;

  DetailPage(this._title);

  @override
  _DetailPageState createState() => _DetailPageState(_title);
}

class _DetailPageState extends State<DetailPage> {
  String _title;
  String _tooltip = 'Cart';
  double _floatingIconSize = 35.0;

  _DetailPageState(this._title);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.orange[50],
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ExtraPage()),
          );
        },
        child: Icon(Icons.add, size: _floatingIconSize, color: Colors.white,),
        backgroundColor: Colors.orange[700],
      ),
      appBar: AppBar(
        title: Text(_title, style: TextStyle(color: Colors.white),),
        backgroundColor: Colors.red[800],
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.room_service),
            iconSize: 45.0,
            tooltip: _tooltip,
            onPressed: (){},
          )
        ],
      ),
      body: DetailWidget(),
    );
  }
}
