import 'package:flutter/material.dart';
import '../models/meal.dart';
import '../widgets/defaultLayout.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'detailsPage.dart';

/// Class containing the item type template
class MealItem extends StatelessWidget {
  final Meal _mealName;
  final double _typeFontSize = 20.0;
  final double _leadingWidgetWidth = 50.0;
  final double _dividerMargin = 20.0;
  MealItem(this._mealName);

  @override
  Widget build(BuildContext context) {
    final String _assetName = 'images/icon_meal.svg';
    final Widget svgMeal =
        new SvgPicture.asset(_assetName, semanticsLabel: 'icon type');
    return Column(
      children: <Widget>[
        ListTile(
          leading: Container(
            width: _leadingWidgetWidth,
            child: svgMeal,
          ),
          title: Text(
            _mealName.name,
            style: TextStyle(
              fontFamily: 'Baskerville',
              fontSize: _typeFontSize,
            ),
          ),
          trailing: Text(
            '\$'+_mealName.price,
            style: TextStyle(
              fontFamily: 'Baskerville',
              fontSize: _typeFontSize,
            ),
          ),
          onTap: (){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => DetailPage(_mealName.name)),
            );
          },
        ),
        Container(
          margin: EdgeInsets.symmetric(vertical: 0.0, horizontal: _dividerMargin),
          child: Divider(
            color: Colors.grey[400],
          ),
        )
      ],
    );
  }
}

///Content widget

class MealListWidget extends StatefulWidget {
  @override
  _MealListWidgetState createState() => _MealListWidgetState();
}

class _MealListWidgetState extends State<MealListWidget> {
  List<Meal> _typeList = <Meal>[
    new Meal("dish1", "10"),
    new Meal("dish2", "20"),
    new Meal("dish3", "30"),
  ];

  double _cellHeight = 75.0;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: ListView.builder(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
        itemCount: _typeList.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            height: _cellHeight,
            child: MealItem(
              _typeList[index],
            ),
          );
        },
      ),
    );
  }
}

///Toolbar widget

class MenuPage extends StatefulWidget {
  final String _type;

  MenuPage(this._type);

  @override
  _MenuPageState createState() => _MenuPageState(_type);
}

class _MenuPageState extends State<MenuPage> {
  String _type;

  _MenuPageState(this._type);

  @override
  Widget build(BuildContext context) {
    return DefaultLayout(_type, MealListWidget());
  }
}
