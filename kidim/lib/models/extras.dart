class Extras {
  String _name;
  String _price;
  bool isSelected;

  Extras(this._name, this._price, this.isSelected);

  String get price => _price;

  String get name => _name;

}