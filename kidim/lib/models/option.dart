class Option {
  String _name;
  String _price;

  Option(this._name, this._price);

  String get price => _price;

  String get name => _name;

}