import 'package:flutter/material.dart';
import 'pages/landingPage.dart';

void main() => runApp(Main());

class Main extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Kidim',
      initialRoute: '/',
      routes: {
        '/': (context) => LandingPage(),
      },
    );
  }
}


