import 'package:flutter/material.dart';

 /// Default Layout is the layout that will be use to make alost all the pages
 /// it consists in a toolbar containing a title the cart item and the body theme color

class DefaultLayout extends StatefulWidget {

  //props required to use the default layout title is the ttile of toolbar
  final String _title;
  //content is the body content
  final Widget _content;

  DefaultLayout(this._title, this._content);

  @override
  _DefaultLayoutState createState() => _DefaultLayoutState(_title, _content);
}

class _DefaultLayoutState extends State<DefaultLayout> {
  String _title;
  Widget _content;
  String _tooltip = 'Cart';

  _DefaultLayoutState(this._title, this._content);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.orange[50],
      appBar: AppBar(
        title: Text(_title, style: TextStyle(color: Colors.white),),
        backgroundColor: Colors.red[800],
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.room_service),
            iconSize: 45.0,
            tooltip: _tooltip,
            onPressed: (){},
          )
        ],
      ),
      body: _content,
    );
  }
}


/*


 */



