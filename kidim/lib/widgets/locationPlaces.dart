import 'package:google_maps_webservice/places.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart' as LocationManager;

const kGoogleApiKey = "AIzaSyDzTh679MhvFR3QXD4CamO71i1-dye0UeQ";
GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomeState();
  }
}

class HomeState extends State<Home> {
  final homeScaffoldKey = GlobalKey<ScaffoldState>();
  List<PlacesSearchResult> places = [];
  bool isLoading = false;
  String errorMessage;
  Map currentLocation = <String, double>{};

  ///refresh was kept only to know how to call getUserLocation
  void refresh() async {
    getUserLocation();
  }

  ///Get user location indirectely forced me to cast getLocation
  ///to map remember to put a break point here

  void getUserLocation() async {
    final location = LocationManager.Location();
    try {
      currentLocation = (await location.getLocation()) as Map<String, double>;
      final lat = currentLocation["latitude"];
      final lng = currentLocation["longitude"];
      final center = LatLng(lat, lng);
      getNearbyPlaces(center);
    } on Exception {
      currentLocation = null;

    }
  }

  ///GetNearbyPlace will give all the information needed to display the potential restaurants
  void getNearbyPlaces(LatLng center) async {
    setState(() {
      this.isLoading = true;
      this.errorMessage = null;
    });

    final location = Location(center.latitude, center.longitude);
    final result = await _places.searchNearbyWithRadius(location, 2500);
    setState(() {
      this.isLoading = false;
      if (result.status == "OK") {
        this.places = result.results;
        result.results.forEach((f) {
          final markerOptions = MarkerOptions(
              position:
                  LatLng(f.geometry.location.lat, f.geometry.location.lng),
              infoWindowText: InfoWindowText("${f.name}", "${f.types?.first}"));
        });
      } else {
        this.errorMessage = result.errorMessage;
      }
    });
  }

  void onError(PlacesAutocompleteResponse response) {
    homeScaffoldKey.currentState.showSnackBar(
      SnackBar(content: Text(response.errorMessage)),
    );
  }

  ///this one is just a container that will be removed later
  @override
  Widget build(BuildContext context) {
    refresh();
    return Scaffold(
      key: homeScaffoldKey,
      body: Container(),
    );
  }
}
